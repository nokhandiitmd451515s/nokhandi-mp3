CREATE TABLE THREAD (ID BIGINT NOT NULL, CREATORID BIGINT, DESCRIPTION VARCHAR(255), SUBJECT VARCHAR(255), PRIMARY KEY (ID))
CREATE TABLE THREADCOMMENT (ID BIGINT NOT NULL, CONTENT VARCHAR(255), TIMESTAMP DATETIME, USERID BIGINT, THREAD_ID BIGINT, PRIMARY KEY (ID))
CREATE TABLE USERS (ID BIGINT NOT NULL, DTYPE VARCHAR(31), PASSWORD VARCHAR(255), USERNAME VARCHAR(255), THREAD_ID BIGINT, PRIMARY KEY (ID))
ALTER TABLE THREADCOMMENT ADD CONSTRAINT FK_THREADCOMMENT_THREAD_ID FOREIGN KEY (THREAD_ID) REFERENCES THREAD (ID) ON DELETE CASCADE
ALTER TABLE USERS ADD CONSTRAINT FK_USERS_THREAD_ID FOREIGN KEY (THREAD_ID) REFERENCES THREAD (ID) ON DELETE CASCADE
CREATE TABLE SEQUENCE (SEQ_NAME VARCHAR(50) NOT NULL, SEQ_COUNT DECIMAL(38), PRIMARY KEY (SEQ_NAME))
INSERT INTO SEQUENCE(SEQ_NAME, SEQ_COUNT) values ('SEQ_GEN', 0)
