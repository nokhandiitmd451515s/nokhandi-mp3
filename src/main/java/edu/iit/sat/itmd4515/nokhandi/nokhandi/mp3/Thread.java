/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.nokhandi.mp3;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import org.eclipse.persistence.annotations.CascadeOnDelete;

/**
 *
 * @author nokhandiar
 */
@Entity
public class Thread implements Serializable{
    @Id @GeneratedValue
    private Long id;
    
    private String subject;
    private String description;
    private Long creatorID;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread")
    @JoinColumn(name = "Admins")
    @CascadeOnDelete
    private List<Admins> adminId;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread")
    @JoinColumn(name = "Comments")
    @CascadeOnDelete
    private List<ThreadComment> comments;

    public Long getId(){
        return id;
    }
    
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Admins> getAdmins() {
        return adminId;
    }

    public void setAdminIDs(List<Admins> admins) {
        this.adminId = admins;
    }

    public Long getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(Long creatorID) {
        this.creatorID = creatorID;
    }

    public List<ThreadComment> getComments() {
        return comments;
    }

    public void setComments(List<ThreadComment> comments) {
        this.comments = comments;
    }
    
    public void addComment(ThreadComment comment){
        comments.add(comment);
    }
    
    public void addAdmin(Admins admin){
        adminId.add(admin);
    }
    
}
