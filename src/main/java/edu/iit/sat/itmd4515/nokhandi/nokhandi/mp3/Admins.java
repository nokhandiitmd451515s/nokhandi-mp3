/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.nokhandi.mp3;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author nokhandiar
 */
@Entity
public class Admins extends Users implements Serializable {

    @ManyToOne
    private Thread thread;

    public void setThread(Thread thread) {
        this.thread = thread;
    }
}