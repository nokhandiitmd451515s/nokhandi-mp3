/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.nokhandi.mp3;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.annotations.CascadeOnDelete;

/**
 *
 * @author nokhandiar
 */
@Entity

public class ThreadComment implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private Long userID;
    private String content;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    //automagically sets timestamp on comment save
    @PrePersist
    private void setTimeStamp(){
        timestamp = GregorianCalendar.getInstance().getTime();
    }
    
    @ManyToOne
    private Thread thread;

    ThreadComment() {
        
    }
    
    public Long getId() {
        return id;
    }

    public Date getTimeStamp(){
        return timestamp;
    }
    public Long getUserID() {
        return userID;
    }

    public String getContent() {
        return content;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Thread getThread() {
        return thread;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    
}
