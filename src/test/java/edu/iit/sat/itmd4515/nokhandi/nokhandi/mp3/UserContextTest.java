/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.nokhandi.mp3;

import javax.persistence.TypedQuery;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nokhandiar
 */
public class UserContextTest extends AbstractJPATest {

    public UserContextTest() {

    }

    @Test
    public void testAdminThread() {
        TypedQuery<Thread> q = em.createQuery("select t from Thread t where t.subject = ?1", Thread.class);
        q.setParameter(1, "Standing on the edge of a rock");
        Thread t = q.getSingleResult();
        assertNotNull(t.getId());
        assertNotNull(t.getAdmins());
    }
}
