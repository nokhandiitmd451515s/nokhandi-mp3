/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.nokhandi.mp3;

import javax.persistence.TypedQuery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nokhandiar
 */
public class ThreadTest extends AbstractJPATest {

    public ThreadTest() {
    }

    @Test
    public void testCreateThread() {
        Thread thread = new Thread();
        thread.setSubject("Hello");
        tx.begin();
        em.persist(thread);
        tx.commit();
        assertTrue(em.contains(thread));
    }

    @Test
    public void testThreadComment(){
        TypedQuery<Thread> q = em.createQuery("select t from Thread t where t.subject = ?1", Thread.class);
        q.setParameter(1, "Standing on the edge of a rock");
        Thread t = q.getSingleResult();
        assertNotNull(t.getId());
        assertNotNull(t.getComments());
        assertNotNull(t.getComments().get(0).getTimeStamp());
    }
    
    @Test
    public void testThreadAdmins(){
        TypedQuery<Thread> q = em.createQuery("select t from Thread t where t.subject = ?1", Thread.class);
        q.setParameter(1, "Standing on the edge of a rock");
        Thread t = q.getSingleResult();
        assertNotNull(t.getId());
        assertNotNull(t.getAdmins());
    }
    @Test
    public void testUpdateThread() {
        TypedQuery<Thread> q = em.createQuery("select t from Thread t where t.subject = ?1", Thread.class);
        q.setParameter(1, "Standing on the edge of a rock");
        Thread t = q.getSingleResult();
        assertNotNull(t.getId());

        String originalName = t.getSubject();
        String newName = "olleH";
        tx.begin();
        t.setSubject(newName);
        tx.commit();

        assertNotEquals(originalName, t.getSubject());
        assertTrue(newName.equals(t.getSubject()));

        tx.begin();
        t.setSubject(originalName);
        tx.commit();
    }
    
    @Test
    public void testDeleteThread(){
        TypedQuery<Thread> q = em.createQuery("select t from Thread t where t.subject = ?1", Thread.class);
        q.setParameter(1, "Standing on the edge of a rock");
        Thread t = q.getSingleResult();
        assertNotNull(t.getId());
        
        tx.begin();
        em.remove(t);
        tx.commit();
        
        assertNull(em.find(Thread.class, 1L));
    }
}
