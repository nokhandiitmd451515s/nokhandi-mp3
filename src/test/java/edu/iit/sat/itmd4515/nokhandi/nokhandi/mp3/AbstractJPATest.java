/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.nokhandi.mp3;

import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author spyrisos
 * @modifiedBy nokhandi
 */
public abstract class AbstractJPATest {

    private static EntityManagerFactory emf;
    protected EntityManager em;
    protected EntityTransaction tx;

    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory("nokhandiPU");
    }

    @AfterClass
    public static void tearDownClass() {
        emf.close();
    }

    @Before
    public void setUp() {
        em = emf.createEntityManager();
        tx = em.getTransaction();
        createTestData();
    }

    private void createTestData() {
        tx.begin();

        Users neil = new Users();
        neil.setPassword("itmd4515");
        neil.setUsername("neil");
        Admins adminNeil = new Admins();
        adminNeil.setUsername("administrator");
        adminNeil.setPassword("itmd4515");

        em.persist(neil);
        em.persist(adminNeil);

        Thread firstThread = new Thread();
        firstThread.setSubject("Standing on the edge of a rock");
        firstThread.setDescription("It's quite large");

        ThreadComment comment = new ThreadComment();
        comment.setContent("Wow thats stupid");

        //sets Neil to having created both thread and comment
        comment.setUserID(neil.getId());
        firstThread.setCreatorID(neil.getId());

        em.persist(firstThread);
        em.persist(comment);
        //attaches comment to thread
        
        comment.setThread(firstThread);
        adminNeil.setThread(firstThread);
        //firstThread.addComment(comment);
        tx.commit();
        firstThread.addComment(comment);
        firstThread.addAdmin(adminNeil);
    }

    @After
    public void tearDown() {
        removeTestData();
        em.close();
    }

    private void removeTestData() {
//        TypedQuery<RadioStation> q = em.createQuery("select r from RadioStation r where r.name = ?1", RadioStation.class);
//        q.setParameter(1, "WTTW");
//        RadioStation r = q.getSingleResult();

//        Show s = em.createNamedQuery("Show.findByName", Show.class).setParameter("name", "Daily News").getSingleResult();
        tx.begin();
//        em.remove(r);
//        em.remove(s);
        for (Users u : em.createQuery("select u from Users u", Users.class).getResultList()) {
            em.remove(u);
        }

        for (ThreadComment tc : em.createQuery("select tc from ThreadComment tc", ThreadComment.class).getResultList()) {
            em.remove(tc);
        }

        for (Thread t : em.createQuery("select t from Thread t", Thread.class).getResultList()) {
            em.remove(t);
        }

        tx.commit();
    }
}
