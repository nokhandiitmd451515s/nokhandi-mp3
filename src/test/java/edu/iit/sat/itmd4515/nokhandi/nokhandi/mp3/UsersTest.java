/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.nokhandi.mp3;

import javax.persistence.TypedQuery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author nokhandiar
 */
public class UsersTest extends AbstractJPATest {
    
    public UsersTest() {
    }
    
    @Test
    public void testCreateUser() {
        Users neil = new Users();
        neil.setUsername("neil");
        neil.setPassword("itmd4515");
        tx.begin();
        em.persist(neil);
        tx.commit();
        assertTrue(em.contains(neil));
    }
    
    @Test
    public void testReadUser(){
        TypedQuery<Users> q = em.createQuery("select u from Users u where u.username = ?1", Users.class);
        q.setParameter(1,"neil");
        Users u = q.getSingleResult();
        assertNotNull(u.getId());
        assertNotNull(u.getUsername());
        assertNotNull(u.getPassword());
    }
    
    @Test
    public void testUpdateUser(){
        TypedQuery<Users> q = em.createQuery("select u from Users u where u.username = ?1", Users.class);
        q.setParameter(1,"neil");
        Users u = q.getSingleResult();
        assertNotNull(u.getId());

        String originalName = u.getUsername();
        String newName = "lien";
        tx.begin();
        u.setUsername(newName);
        tx.commit();

        assertNotEquals(originalName, u.getUsername());
        assertTrue(newName.equals(u.getUsername()));

        tx.begin();
        u.setUsername(originalName);
        tx.commit();
    }
    
    @Test
    public void testDeleteUser(){
        TypedQuery<Users> q = em.createQuery("select r from Users r where r.username = ?1", Users.class);
        q.setParameter(1, "neil");
        Users r = q.getSingleResult();
        assertNotNull(r.getId());
        
        tx.begin();
        em.remove(r);
        tx.commit();
        
        assertNull(em.find(Users.class, 1L));
    }
    
}
